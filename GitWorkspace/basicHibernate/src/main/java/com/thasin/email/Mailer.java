package com.thasin.email;

import java.util.*;

import javax.mail.Session;
import javax.mail.internet.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;

public class Mailer {
	public static void send(final String from, final String password, String to, String sub, List<Email> data) {
		Email email = null;
		String s = "TransactionDetails\n";
		StringBuilder status = new StringBuilder();
		status = status.append("Hi Team,\nThe below transactions got failed.\n");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);
			BodyPart message1 = new MimeBodyPart(); 
			BodyPart message2 = new MimeBodyPart();
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(sub);
			Iterator<Email> itr = data.iterator();
			while (itr.hasNext()) {

				System.out.println("1");
				email = itr.next();
				s = "Email : " + email.getEmail() + "\nName : " + email.getName() + "\n";
				status = status.append(s);
				// message.setDescription("Email name: "+email.getEmail()+"\nName:
				// "+email.getName()+"\n");
			}
			DataSource source = new FileDataSource("D:\\Email data mail\\Sample.pdf");
			
			
			message1.setText(status.toString());
			message2.setDataHandler(new DataHandler(source));
			message2.setFileName("FailedTtansaction.pdf");
			Multipart multi = new MimeMultipart();
			multi.addBodyPart(message1);
			multi.addBodyPart(message2);
			message.setContent(multi);
			System.out.println(status);
			Transport.send(message);
			
			System.out.println("message sent successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
