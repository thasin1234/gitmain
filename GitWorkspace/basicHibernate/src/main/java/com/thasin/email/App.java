package com.thasin.email;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.itextpdf.text.Document;

public class App {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();

		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		/*
		 * Email e = new Email(); e.setEmail("thasin422@gmail.com");
		 * e.setName("Thasin"); session.save(e);
		 */
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Email> criteria = builder.createQuery(Email.class);
		criteria.from(Email.class);

		List<Email> data = session.createQuery(criteria).getResultList();
		System.out.println("Do you want to Generate the PDF and store the values press 1 or Email press 2");
		int n = Integer.parseInt(br.readLine());
		switch(n)
		{
		case 1 : 
			pdf.PDFGeneration(data);
			break;
		case 2 : 
			Mailer.send("thasin422@gmail.com", "9944460823", "mohammedyasin24@gmail.com", "FailedTransaction", data);
			break;
		}
		
		/*Iterator<Email> itr = data.iterator();
		try {
			PDDocument pdfdoc = new PDDocument();
			pdfdoc.addPage(new PDPage());

			// path where the PDF file will be store
			pdfdoc.save("D:\\Email data mail\\Sample.pdf");
			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream("D:\\Email data mail\\Sample.pdf"));
			doc.open();
			while (itr.hasNext()) {
				e1 = itr.next();
				System.out.println("Name" + e1.getName() + "\nEmail" + e1.getEmail());
				doc.add(new Paragraph("Name" + e1.getName() + "Email" + e1.getEmail()));

			}
			doc.close();
			writer.close();
			pdfdoc.close();

		} catch (Exception e) {
			System.out.println(e);
		}*/

		t.commit();
		System.out.println("Save");

	}
}
